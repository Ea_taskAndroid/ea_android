package com.app.hemdan.ea.api;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mhemdan on 28/02/17.
 */
public class ApiClient {
    private static ApiClient ourInstance = new ApiClient();
    private static Retrofit retrofit;

    public static ApiClient getInstance() {
        return ourInstance;
    }

    private ApiClient() {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        Interceptor languageHeaderInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .addHeader(URLS.HEADER_ACCEPT_NAME  , URLS.HEADER_ACCEPT_VALUE)
                        .addHeader(URLS.HEADER_CONTENT_NAME , URLS.HEADER_CONTENT_VALUE)
                        .build();

                return chain.proceed(request);
            }
        };

        httpClient.addInterceptor(languageHeaderInterceptor);



        httpClient.connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(URLS.BASE_URL)
                        .addConverterFactory(
                                GsonConverterFactory.create()
                        );

        retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();

    }

    public Retrofit getRestAdapter(){
        return retrofit;
    }

}
