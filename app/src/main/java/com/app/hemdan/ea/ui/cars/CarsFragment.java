package com.app.hemdan.ea.ui.cars;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.hemdan.ea.R;
import com.app.hemdan.ea.base.fragment.BaseFragment;
import com.app.hemdan.ea.base.fragment.TransparentLoadingFragment;
import com.app.hemdan.ea.ui.cars.model.CarModel;
import com.mirhoseini.utils.Utils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by m.hemdan on 7/7/18.
 */

public class CarsFragment extends BaseFragment implements CarsView {
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private CarsAdapter adapter = new CarsAdapter();
    private CarsPresenter presenter = new CarsPresenterImpl();
    public static void navigate(FragmentTransaction transaction,int resId){
        CarsFragment fragment = new CarsFragment();
        transaction.replace(resId,fragment,fragment.getClass().getSimpleName())
                .commitNowAllowingStateLoss();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cars,container,false);
        ButterKnife.bind(this,view);
        init();
        return view;
    }
    private void init(){
        list.setAdapter(adapter);
        presenter.bind(this);
        presenter.getCars(Utils.isConnected(getContext()),true,false);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getCars(Utils.isConnected(getContext()),false,false);
            }
        });
    }

    @Override
    public void insertItems(List<CarModel> carModels) {
        swipeRefreshLayout.setRefreshing(false);
        adapter.insertItems(carModels);
    }

    @Override
    public void setAutoRefreshRate(final int minutes) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(timer != null) {
                    return;
                }
                timer = new Timer();
                timer.scheduleAtFixedRate(timerTask, 0, minutes * 60 * 1000);
            }
        },minutes * 60 * 1000);
    }

    private Timer timer;
    private TimerTask timerTask = new TimerTask() {

        @Override
        public void run() {
            if(isForeGround)
                presenter.getCars(Utils.isConnected(getContext()),false,true);
        }
    };

    private void stopTimer(){
        if(timer!=null) {
            timer.cancel();
            timer = null;
        }
    }

    private boolean isForeGround = true;
    @Override
    public void onResume() {
        super.onResume();
        isForeGround = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isForeGround = false;
    }

    @Override
    public void onDestroy() {
        presenter.unbind();
        super.onDestroy();
    }


    @Override
    public void onStop() {
        super.onStop();
        stopTimer();
    }

    @Override
    public void showLoading() {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_loading,
                new TransparentLoadingFragment(),TransparentLoadingFragment.class.getName());
        fragmentTransaction.commit();
    }

    @Override
    public void hideLoading() {
        if(isAdded()) {
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction =
                    fragmentManager.beginTransaction();
            Fragment fragment = fragmentManager.findFragmentByTag(TransparentLoadingFragment.class.getName());
            if (fragment != null) {
                fragmentTransaction.remove(fragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        }
    }
}
