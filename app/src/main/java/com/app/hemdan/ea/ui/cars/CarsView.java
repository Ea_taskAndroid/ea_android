package com.app.hemdan.ea.ui.cars;

import com.app.hemdan.ea.base.BaseView;
import com.app.hemdan.ea.ui.cars.model.CarModel;

import java.util.List;

/**
 * Created by m.hemdan on 7/7/18.
 */

public interface CarsView extends BaseView {
    void insertItems(List<CarModel> carModels);
    void setAutoRefreshRate(int minutes);
}
