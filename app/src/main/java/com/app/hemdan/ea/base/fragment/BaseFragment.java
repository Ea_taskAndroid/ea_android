package com.app.hemdan.ea.base.fragment;

import android.support.v4.app.Fragment;

import com.app.hemdan.ea.base.BaseActivity;
import com.app.hemdan.ea.base.BaseView;


/**
 * Created by m.hemdan on 1/19/18.
 */

public class BaseFragment extends Fragment implements BaseView {
    @Override
    public void showLoading() {
        if(getActivity()!=null)
            ((BaseActivity)getActivity()).showLoading();
    }

    @Override
    public void hideLoading() {
        if(getActivity()!=null)
            ((BaseActivity)getActivity()).hideLoading();
    }

    @Override
    public void showError(String errorMessage) {
        if(getActivity()!=null)
            ((BaseActivity)getActivity()).showError(errorMessage);
    }

    @Override
    public void showOfflineError() {
        ((BaseActivity)getActivity()).showOfflineError();
    }
}
