package com.app.hemdan.ea.ui.cars.api;

import com.app.hemdan.ea.api.URLS;
import com.app.hemdan.ea.ui.cars.model.CarsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by m.hemdan on 7/7/18.
 */

public interface CarsApi {
    String LAST_RECEIVED_TICKS = "Ticks";
    @GET(URLS.CARS)
    Call<CarsResponse> getCars(@Query(LAST_RECEIVED_TICKS) String lastReceivedTicks);
}
