package com.app.hemdan.ea.ui.cars;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.app.hemdan.ea.R;
import com.app.hemdan.ea.base.BaseActivity;
import com.cocosw.bottomsheet.BottomSheet;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        attachCars();
    }
    private void attachCars(){
        CarsFragment.navigate(getSupportFragmentManager().beginTransaction(),
                R.id.fragment_container);
    }

    @OnClick(R.id.container_sort)
    public void onContainerSortClick(View view){
        new BottomSheet.Builder(this, R.style.BottomSheet_Dialog)
                .title(getResources().getString(R.string.app_sort_by))
                .sheet(R.menu.bottom_sheet_sort)
                .listener(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO
                    }
                }).show();
    }
}
