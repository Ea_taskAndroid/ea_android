package com.app.hemdan.ea.api;

/**
 * Created by m.hemdan on 9/16/17.
 */

public interface URLS {



    String BASE_URL               = "http://api.emiratesauction.com/v2/";
    String HEADER_ACCEPT_NAME     = "accept";
    String HEADER_ACCEPT_VALUE    = "application/json";
    String HEADER_CONTENT_NAME    = "content-type";
    String HEADER_CONTENT_VALUE   = "application/json";
    String CARS                   = "carsonline";


}
