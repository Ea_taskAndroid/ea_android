package com.app.hemdan.ea.base;

/**
 * Created by m.hemdan on 9/16/17.
 */
public interface BasePresenter<T> {

    void bind(T view);

    void unbind();

}
