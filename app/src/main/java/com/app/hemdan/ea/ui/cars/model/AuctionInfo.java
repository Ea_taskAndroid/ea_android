package com.app.hemdan.ea.ui.cars.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class AuctionInfo {

    private final String ARABIC_LANGUAGE = "ar";
    private final String ENGLISH_LANGUAGE = "en";

    @SerializedName("bids")
@Expose
private Integer bids;
@SerializedName("endDate")
@Expose
private Integer endDate;
@SerializedName("endDateEn")
@Expose
private String endDateEn;
@SerializedName("endDateAr")
@Expose
private String endDateAr;
@SerializedName("currencyEn")
@Expose
private String currencyEn;
@SerializedName("currencyAr")
@Expose
private String currencyAr;
@SerializedName("currentPrice")
@Expose
private Integer currentPrice;
@SerializedName("minIncrement")
@Expose
private Integer minIncrement;
@SerializedName("lot")
@Expose
private Integer lot;
@SerializedName("priority")
@Expose
private Integer priority;
@SerializedName("VATPercent")
@Expose
private Integer vATPercent;
@SerializedName("isModified")
@Expose
private Integer isModified;
@SerializedName("itemid")
@Expose
private Integer itemid;
@SerializedName("iCarId")
@Expose
private Integer iCarId;
@SerializedName("iVinNumber")
@Expose
private String iVinNumber;

public Integer getBids() {
return bids;
}

public void setBids(Integer bids) {
this.bids = bids;
}

public Integer getEndDate() {
return endDate;
}
public Long getTimeLeft(String language){
    return getDateInMilli(getEndDateEn()) - Calendar.getInstance(Locale.ENGLISH).getTimeInMillis();
}
public String getEndDate(String language){
    return language.equals(ARABIC_LANGUAGE)? getEndDateAr() : getEndDateEn();
}
public void setEndDate(Integer endDate) {
this.endDate = endDate;
}

public String getEndDateEn() {
    return endDateEn;
}
public Long getDateInMilli(String dateString){
    String dateFormat  = "d MMM hh:mm aa";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat,Locale.ENGLISH);
    try {
        Date date = simpleDateFormat.parse(dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        calendar.set(Calendar.YEAR,Calendar.getInstance().get(Calendar.YEAR));
        return calendar.getTimeInMillis();
    } catch (ParseException e) {
        e.printStackTrace();
    }
    return null;
}

public void setEndDateEn(String endDateEn) {
this.endDateEn = endDateEn;
}

public String getEndDateAr() {
return endDateAr;
}

public void setEndDateAr(String endDateAr) {
this.endDateAr = endDateAr;
}

public String getCurrencyEn() {
return currencyEn;
}

public String getCurrency(String language){
    return language.equals(ARABIC_LANGUAGE)? getCurrencyAr() : getCurrencyEn();
}
public void setCurrencyEn(String currencyEn) {
this.currencyEn = currencyEn;
}

public String getCurrencyAr() {
return currencyAr;
}

public void setCurrencyAr(String currencyAr) {
this.currencyAr = currencyAr;
}

public Integer getCurrentPrice() {
return currentPrice;
}

public void setCurrentPrice(Integer currentPrice) {
this.currentPrice = currentPrice;
}

public Integer getMinIncrement() {
return minIncrement;
}

public void setMinIncrement(Integer minIncrement) {
this.minIncrement = minIncrement;
}

public Integer getLot() {
return lot;
}

public void setLot(Integer lot) {
this.lot = lot;
}

public Integer getPriority() {
return priority;
}

public void setPriority(Integer priority) {
this.priority = priority;
}

public Integer getVATPercent() {
return vATPercent;
}

public void setVATPercent(Integer vATPercent) {
this.vATPercent = vATPercent;
}

public Integer getIsModified() {
return isModified;
}

public void setIsModified(Integer isModified) {
this.isModified = isModified;
}

public Integer getItemid() {
return itemid;
}

public void setItemid(Integer itemid) {
this.itemid = itemid;
}

public Integer getICarId() {
return iCarId;
}

public void setICarId(Integer iCarId) {
this.iCarId = iCarId;
}

public String getIVinNumber() {
return iVinNumber;
}

public void setIVinNumber(String iVinNumber) {
this.iVinNumber = iVinNumber;
}

}