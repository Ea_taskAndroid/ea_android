package com.app.hemdan.ea.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.app.hemdan.ea.R;


//import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by m.hemdan on 11/30/17.
 */

public class TransparentLoadingFragment extends Fragment {
    public static String FRAGMENT_TAG = "TransparentLoadingFragment";

    @BindView(R.id.progress)
    DilatingDotsProgressBar progress;

    private static View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_loading_transparent,container, false);
            ButterKnife.bind(this, view);
            rotate();
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }
        return view;
    }


    private void rotate() {
        // show progress bar and start animating
        progress.showNow();
    }
}