package com.app.hemdan.ea.base;

import android.support.v7.app.AppCompatActivity;

import com.app.hemdan.ea.base.fragment.LoadingFragmentDialog;
import com.tapadoo.alerter.Alerter;
import com.app.hemdan.ea.R;

/**
 * Created by m.hemdan on 7/7/18.
 */

public class BaseActivity extends AppCompatActivity implements BaseView {
    @Override
    public void showLoading() {
        LoadingFragmentDialog fragment = (LoadingFragmentDialog) getSupportFragmentManager().findFragmentByTag(LoadingFragmentDialog.FRAGMENT_TAG);
        if (fragment == null) {
            fragment = new LoadingFragmentDialog();
            fragment.setCancelable(false);
            getSupportFragmentManager().beginTransaction()
                    .add(fragment, LoadingFragmentDialog.FRAGMENT_TAG)
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }
    }

    @Override
    public void hideLoading() {
        LoadingFragmentDialog fragment = (LoadingFragmentDialog) getSupportFragmentManager().findFragmentByTag(LoadingFragmentDialog.FRAGMENT_TAG);
        if (fragment != null) {
            // fragment.dismissAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void showError(String errorMessage) {
        Alerter.create(this)
                .setText(errorMessage)
                .setBackgroundColorRes(R.color.colorPrimary)
                .show();
    }

    @Override
    public void showOfflineError() {

    }
}
