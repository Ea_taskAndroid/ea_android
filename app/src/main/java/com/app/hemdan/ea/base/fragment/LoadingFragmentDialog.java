package com.app.hemdan.ea.base.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;

import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.app.hemdan.ea.R;

//import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by m.hemdan on 8/30/17.
 */

public class LoadingFragmentDialog extends DialogFragment {
    public static String FRAGMENT_TAG = "LoadingFragmentDialog";

    @BindView(R.id.progress)
    DilatingDotsProgressBar progress;
    @Override
    public void onResume()
    {
        super.onResume();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(DialogFragment.STYLE_NO_TITLE);
        View view = View.inflate(getActivity(), R.layout.dialog_loading_fragment, null);
        dialog.setContentView(view);
        ButterKnife.bind(this,view);
        rotate();
        return dialog;
    }
    private void rotate(){
        // show progress bar and start animating
        progress.showNow();
    }
}
