package com.app.hemdan.ea.base;

/**
 * Created by m.hemdan on 7/7/18.
 */

public interface BaseView {
    void showLoading();
    void hideLoading();
    void showError(String errorMessage);
    void showOfflineError();
}
