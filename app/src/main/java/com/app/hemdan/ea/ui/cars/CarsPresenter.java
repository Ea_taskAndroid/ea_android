package com.app.hemdan.ea.ui.cars;

import com.app.hemdan.ea.base.BasePresenter;

/**
 * Created by m.hemdan on 7/7/18.
 */

public interface CarsPresenter extends BasePresenter<CarsView> {
    void getCars(boolean isConnected,boolean canShowLoading,boolean isAutoRefresh);
}
