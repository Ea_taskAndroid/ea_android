package com.app.hemdan.ea.ui.cars;

import com.app.hemdan.ea.api.ApiClient;
import com.app.hemdan.ea.ui.cars.api.CarsApi;
import com.app.hemdan.ea.ui.cars.model.CarsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by m.hemdan on 7/7/18.
 */

public class CarsPresenterImpl implements CarsPresenter {
    private CarsView view;
    private static String lastReceivedTicks = null;
    @Override
    public void bind(CarsView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;
    }

    @Override
    public void getCars(boolean isConnected , final boolean canShowLoading,boolean isAutoRefresh) {
        if(!isConnected){
            view.showOfflineError();
            return;
        }

        if(canShowLoading)
            view.showLoading();
        final CarsApi apiInterface =  ApiClient.getInstance().getRestAdapter()
                .create(CarsApi.class);

        Callback<CarsResponse> callback = new Callback<CarsResponse>() {
            @Override
            public void onResponse(Call<CarsResponse> call, Response<CarsResponse> response) {
                if(view!=null) {
                    if(canShowLoading)
                        view.hideLoading();
                    if(response != null && response.body() != null)
                        if(response.body()!= null){
                            lastReceivedTicks = response.body().getTicks();
                            view.insertItems(response.body().getCars());
                            view.setAutoRefreshRate(response.body().getRefreshInterval());
                        }
                }
            }

            @Override
            public void onFailure(Call<CarsResponse> call, Throwable t) {
                view.hideLoading();
            }
        };
        Call<CarsResponse> call = apiInterface.getCars(isAutoRefresh? lastReceivedTicks : null);
        call.enqueue(callback);
    }
}
