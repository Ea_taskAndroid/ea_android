package com.app.hemdan.ea.ui.cars;

import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.hemdan.ea.R;
import com.app.hemdan.ea.ui.cars.model.CarModel;
import com.bumptech.glide.Glide;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by m.hemdan on 7/7/18.
 */

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.CarViewHolder> {
    private List<CarModel> items = new ArrayList<>();
    private final long FIVE_MINUTES_MARGIN =  5 * 60 * 1000;
    public void insertItems(List<CarModel> newItems){
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }
    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_car, parent, false));
    }

    @Override
    public void onBindViewHolder(final CarViewHolder holder, int position) {
            holder.bind(items.get(position));
        if (holder.timer != null) {
            holder.timer.cancel();
        }
        holder.timer = new CountDownTimer(items.get(position).getAuctionInfo().getTimeLeft(Locale.getDefault().getLanguage()), 500) {
            public void onTick(long millisUntilFinished) {
//
                if(millisUntilFinished < FIVE_MINUTES_MARGIN){
                    holder.txtTimeLeft.setTextColor(ContextCompat.getColor(holder.itemView.getContext(),R.color.app_red));
                }else{
                    holder.txtTimeLeft.setTextColor(ContextCompat.getColor(holder.itemView.getContext(),R.color.black));
                }
                holder.txtTimeLeft.setText((new SimpleDateFormat("dd:hh:mm:ss")).format(new Date(millisUntilFinished)));
            }

            public void onFinish() {
                holder.txtTimeLeft.setText("00:00:00");
            }
        }.start();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class CarViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_car)
        ImageView imgCar;
        @BindView(R.id.txt_car_name)
        TextView txtCarName;
        @BindView(R.id.txt_car_price)
        TextView txtCarPrice;
        @BindView(R.id.txt_currency)
        TextView txtCurrency;
        @BindView(R.id.txt_lots)
        TextView txtLots;
        @BindView(R.id.txt_bids)
        TextView txtBids;
        @BindView(R.id.txt_time_left)
        TextView txtTimeLeft;
        CountDownTimer timer;

        public CarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
        public void  bind(CarModel model){
            Glide.with(itemView.getContext()).load(model.getImage()).into(imgCar);
            txtCarName.setText(model.getTitle(Locale.getDefault().getLanguage()));
            txtCarPrice.setText(model.getAuctionInfo().getCurrentPrice().toString());
            txtCurrency.setText(model.getAuctionInfo().getCurrency(Locale.getDefault().getLanguage()));
            txtLots.setText(model.getAuctionInfo().getLot().toString());
            txtBids.setText(model.getAuctionInfo().getBids().toString());
        }
    }
}
